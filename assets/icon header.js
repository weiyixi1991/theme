<style>
<div class="logo-area__right__inner">
          
            
              <a class="header-account-link" href="/account/login" aria-label="Account">
                <span class="desktop-only">Account</span>
                <span class="mobile-only"><svg width="19px" height="18px" viewBox="-1 -1 21 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <g transform="translate(0.968750, -0.031250)" stroke="none" stroke-width="1" fill="currentColor" fill-rule="nonzero">
    <path d="M9,7.5 C10.704,7.5 12.086,6.157 12.086,4.5 C12.086,2.843 10.704,1.5 9,1.5 C7.296,1.5 5.914,2.843 5.914,4.5 C5.914,6.157 7.296,7.5 9,7.5 Z M9,9 C6.444,9 4.371,6.985 4.371,4.5 C4.371,2.015 6.444,0 9,0 C11.556,0 13.629,2.015 13.629,4.5 C13.629,6.985 11.556,9 9,9 Z M1.543,18 L0,18 L0,15 C0,12.377 2.187,10.25 4.886,10.25 L14.143,10.25 C16.273,10.25 18,11.929 18,14 L18,18 L16.457,18 L16.457,14 C16.457,12.757 15.421,11.75 14.143,11.75 L4.886,11.75 C3.04,11.75 1.543,13.205 1.543,15 L1.543,18 Z"></path>
  </g>
</svg></span>
              </a>
            
          
          <a class="show-search-link" href="/search">
            <span class="show-search-link__text">Search</span>
            <span class="show-search-link__icon"><svg viewBox="0 0 19 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" stroke="none" stroke-width="1" fill="currentColor" fill-rule="nonzero">
  <g transform="translate(0.000000, 0.472222)">
    <path d="M14.3977778,14.0103889 L19,19.0422222 L17.8135556,20.0555556 L13.224,15.0385 C11.8019062,16.0671405 10.0908414,16.619514 8.33572222,16.6165556 C3.73244444,16.6165556 0,12.8967778 0,8.30722222 C0,3.71766667 3.73244444,0 8.33572222,0 C12.939,0 16.6714444,3.71977778 16.6714444,8.30722222 C16.6739657,10.4296993 15.859848,12.4717967 14.3977778,14.0103889 Z M8.33572222,15.0585556 C12.0766111,15.0585556 15.1081667,12.0365 15.1081667,8.30827778 C15.1081667,4.58005556 12.0766111,1.558 8.33572222,1.558 C4.59483333,1.558 1.56327778,4.58005556 1.56327778,8.30827778 C1.56327778,12.0365 4.59483333,15.0585556 8.33572222,15.0585556 Z"></path>
  </g>
</svg>

</span>
          </a>
          <a href="/cart" class="cart-link">
            <span class="cart-link__label">Cart</span>{
              display:none;
            }
            <span class="cart-link__icon"><svg viewBox="0 0 21 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" stroke="none" stroke-width="1" fill="currentColor" fill-rule="nonzero">
  <g transform="translate(-0.500000, 0.500000)">
    <path d="M10.5,5.75 L10.5,0 L12,0 L12,5.75 L21.5,5.75 L17.682,17.75 L4.318,17.75 L0.5,5.75 L10.5,5.75 Z M2.551,7.25 L5.415,16.25 L16.585,16.25 L19.449,7.25 L2.55,7.25 L2.551,7.25 Z"></path>
  </g>
</svg></span>
          </a>
        </div>
</style>